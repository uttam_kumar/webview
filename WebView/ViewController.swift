//
//  ViewController.swift
//  WebView
//
//  Created by Syncrhonous on 5/2/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit
import WebKit
class ViewController: UIViewController {

    @IBOutlet var webView: WKWebView!
    
    let myUrl=URL(string: "https://www.prothomalo.com")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let request=URLRequest(url: myUrl!)
        webView.load(request)
    }


}

